# vue3_minimum

Vue3 + Vite

## How I did 

[installation](https://vuejs.org/guide/quick-start.html#try-vue-online)

```sh
npm init vue@latest
```
✔ Project name: vue3_minimum
✔ Add TypeScript? No
✔ Add JSX Support? No
✔ Add Vue Router for Single Page Application development? No
✔ Add Pinia for state management? No
✔ Add Vitest for Unit testing? No
✔ Add Cypress for both Unit and End-to-End testing? No
✔ Add ESLint for code quality? No
✔ Add Prettier for code formatting? No

Scaffolding project in ./vue3_minimum
Done.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

